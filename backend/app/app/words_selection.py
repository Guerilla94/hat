from typing import List

from sqlalchemy import select
from sqlalchemy.orm import Session
from sqlalchemy.sql.expression import func

from app.models import DictionaryWord, category_word_table


def select_random_words(db: Session, categories_ids: List[int], count: int) -> List[DictionaryWord]:
    query = select(DictionaryWord)\
        .where(category_word_table.columns.category_id.in_(categories_ids))\
        .order_by(func.random())\
        .limit(count)\
        .join(category_word_table)

    return db.scalars(query).all()
