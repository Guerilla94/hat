from typing import Iterable


def duplicates(items: Iterable) -> set:
    seen_items = set()
    result = set()

    for item in items:
        if item in seen_items:
            result.add(item)
        seen_items.add(item)

    return result
