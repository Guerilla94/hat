from typing import Any

from sqlalchemy.ext.declarative import as_declarative, declared_attr
from fastapi.encoders import jsonable_encoder


@as_declarative()
class Base:
    id: Any
    __name__: str

    # Generate __tablename__ automatically
    @declared_attr
    def __tablename__(cls) -> str:
        return cls.__name__.lower()

    def dict(self):
        model_dict = self.__dict__.copy()
        del model_dict['_sa_instance_state']

        return model_dict

    @classmethod
    def from_data(cls, data):
        data_dict = jsonable_encoder(data)
        return cls(**data_dict)  # type: ignore
