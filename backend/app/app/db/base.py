# Import all the models, so that Base has them before being
# imported by Alembic
from app.db.base_class import Base  # noqa
from app.models.user import User  # noqa
from app.models.dictionary import DictionaryWord, DictionaryCategory  # noqa
from app.models.player import Player  # noqa
from app.models.stage import Stage  # noqa
from app.models.game import Game, GamePlayer, GameStage # noqa
