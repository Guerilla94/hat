from typing import List

from sqlalchemy import select, or_
from sqlalchemy.orm import Session

from app.crud.base import CRUDBase
from app.models.player import Player
from app.schemas import PlayerCreate, PlayerUpdate


class CRUDPlayer(CRUDBase[Player, PlayerCreate, PlayerUpdate]):
    def get_existing_aliases(self, db: Session, aliases: List[str], created_by_user_id: int) -> List[str]:
        query = select(Player.alias)\
            .where(Player.created_by_user_id == created_by_user_id)\
            .where(Player.alias.in_(aliases))

        return db.scalars(query).all()

    def get_for_user(self, db: Session, user_id: int) -> List[Player]:
        query = select(Player)\
            .where(Player.created_by_user_id == user_id)\
            .where(
                or_(Player.user_id.is_(None), Player.user_id != user_id)
            ).\
            order_by(Player.alias)

        return db.scalars(query).all()


player = CRUDPlayer(Player)
