from app.crud.base import CRUDBase
from app.models import DictionaryCategory
from app.schemas import DictionaryCategoryCreate, DictionaryCategoryUpdate


class CRUDDictionaryCategory(CRUDBase[DictionaryCategory, DictionaryCategoryCreate, DictionaryCategoryUpdate]):
    pass


dictionary_category = CRUDDictionaryCategory(DictionaryCategory)
