from sqlalchemy import Column, String, Integer, SmallInteger, DateTime, Table, ForeignKey
from sqlalchemy.orm import relationship

from app.db.base_class import Base


class GamePlayer(Base):
    __tablename__ = "game_player"
    game_id = Column(ForeignKey("game.id"), primary_key=True)
    player_id = Column(ForeignKey("player.id"), primary_key=True)
    team_name = Column(String(100))

    game = relationship("Game", back_populates="players")
    player = relationship("Player", back_populates="games")


class GameStage(Base):
    __tablename__ = "game_stage"
    game_id = Column(ForeignKey("game.id"), primary_key=True)
    stage_id = Column(ForeignKey("stage.id"), primary_key=True)
    order = Column(SmallInteger, nullable=False)

    game = relationship("Game", back_populates="stages")
    stage = relationship("Stage", backref="games")


game_word_table = Table(
    "game_word",
    Base.metadata,
    Column("game_id", ForeignKey("game.id"), primary_key=True),
    Column("word_id", ForeignKey("dictionary_word.id"), primary_key=True),
)


class Game(Base):
    id = Column(Integer, primary_key=True, index=True)
    start_time = Column(DateTime(), nullable=False)
    end_time = Column(DateTime)

    players = relationship("GamePlayer", back_populates="game")
    stages = relationship("GameStage", back_populates="game")
    words = relationship("DictionaryWord", secondary=game_word_table)
