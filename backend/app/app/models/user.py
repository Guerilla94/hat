from sqlalchemy import Boolean, Column, Integer, String
from sqlalchemy.orm import relationship

from app.db.base_class import Base


class User(Base):
    id = Column(Integer, primary_key=True, index=True)
    first_name = Column(String)
    last_name = Column(String)
    email = Column(String, unique=True, index=True, nullable=False)
    hashed_password = Column(String, nullable=False)
    is_active = Column(Boolean, default=True)
    is_superuser = Column(Boolean, default=False)

    player = relationship("Player", back_populates="user", uselist=False, foreign_keys="[Player.user_id]")
    created_players = relationship(
        "Player",
        back_populates="created_by_user",
        foreign_keys="[Player.created_by_user_id]"
    )
