from sqlalchemy import Column, Integer, String, ForeignKey, UniqueConstraint
from sqlalchemy.orm import relationship

from app.db.base_class import Base


class Player(Base):
    id = Column(Integer, primary_key=True, index=True)
    alias = Column(String(length=50), nullable=False)

    user_id = Column(Integer, ForeignKey("user.id"), unique=True)
    user = relationship("User", back_populates="player", foreign_keys=[user_id])

    created_by_user_id = Column(Integer, ForeignKey("user.id"), nullable=False)
    created_by_user = relationship("User", back_populates="created_players", foreign_keys=[created_by_user_id])
    games = relationship("GamePlayer", back_populates="player")

    __table_args__ = (
        UniqueConstraint(alias, user_id, created_by_user_id),
    )

