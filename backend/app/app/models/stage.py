from sqlalchemy import Column, Integer, SmallInteger, String, Text, DateTime

from app.db.base_class import Base


class Stage(Base):
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(length=100), nullable=False, unique=True)
    description = Column(Text, nullable=False)
    default_order = Column(SmallInteger)
    deleted_at = Column(DateTime)
