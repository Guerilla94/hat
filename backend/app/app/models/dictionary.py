from sqlalchemy import Column, Integer, String, DateTime, ForeignKey, Table

from app.db.base_class import Base

category_word_table = Table(
    "dictionary_category_word",
    Base.metadata,
    Column("category_id", ForeignKey("dictionary_category.id"), primary_key=True),
    Column("word_id", ForeignKey("dictionary_word.id"), primary_key=True)
)


class DictionaryCategory(Base):
    __tablename__ = 'dictionary_category'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, nullable=False)


class DictionaryWord(Base):
    __tablename__ = 'dictionary_word'

    id = Column(Integer, primary_key=True, index=True)
    word = Column(String, nullable=False)
    deleted_at = Column(DateTime)
