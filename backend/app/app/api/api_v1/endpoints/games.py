from datetime import datetime

from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
from sqlalchemy import select

from app.api import deps
from app.api.deps import get_current_user
from app.models import Game, DictionaryWord, category_word_table
from app.models.game import GamePlayer, GameStage, game_word_table
from app.schemas import GameCreate
from app.words_selection import select_random_words

router = APIRouter(
    dependencies=[Depends(get_current_user)]
)


@router.post("/")
def create_game(
    game_data: GameCreate,
    db: Session = Depends(deps.get_db)
):
    game = Game()
    game.start_time = datetime.now()

    game.players = [GamePlayer.from_data(player_data) for player_data in game_data.players]

    for index, stage_id in enumerate(game_data.stages_ids):
        game_stage = GameStage()
        game_stage.stage_id = stage_id
        game_stage.order = index
        game.stages.append(game_stage)

    game.words = select_random_words(db, game_data.dictionary_categories_ids, game_data.words_count)

    db.add(game)
    db.commit()

    # return crud.stage.get_all(db, order_by="name")
