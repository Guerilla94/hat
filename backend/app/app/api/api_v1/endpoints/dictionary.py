from typing import List

from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from app import crud
from app.api import deps
from app.schemas import DictionaryCategory

router = APIRouter()


@router.get("/", response_model=List[DictionaryCategory])
def read_categories(
    db: Session = Depends(deps.get_db)
):
    return crud.dictionary_category.get_all(db, order_by="name")

