from typing import List

from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from app import crud
from app.api import deps
from app.schemas.stage import Stage

router = APIRouter()


@router.get("/", response_model=List[Stage])
def read_stages(
    db: Session = Depends(deps.get_db)
):
    return crud.stage.get_all(db, order_by="name")
