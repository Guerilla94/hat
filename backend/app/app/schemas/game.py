from typing import Optional, List

from app.schemas.base import BaseSchema


class Game(BaseSchema):
    id: int
    start_time: str
    end_time: Optional[str]

    class Config:
        orm_mode = True


class GamePlayerCreate(BaseSchema):
    player_id: int
    team_name: str


class GameCreate(BaseSchema):
    players: List[GamePlayerCreate]
    stages_ids: List[int]
    dictionary_categories_ids: List[int]
    words_count: int


class GameUpdate(BaseSchema):
    id: int
