from pydantic import BaseModel, validator


class BaseSchema(BaseModel):
    @validator('*', pre=True)
    def empty_str_to_none(cls, value):
        if value == '':
            return None
        return value
