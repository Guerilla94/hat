from typing import Optional

from app.schemas.base import BaseSchema


class StageBase(BaseSchema):
    name: str
    description: str
    default_order: Optional[int]


class Stage(StageBase):
    id: int

    class Config:
        orm_mode = True


class StageCreate(BaseSchema):
    pass


class StageUpdate(BaseSchema):
    id: int
