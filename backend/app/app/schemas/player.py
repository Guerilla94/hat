from typing import Optional

from app.schemas.base import BaseSchema


class PlayerBase(BaseSchema):
    alias: str


class PlayerCreate(PlayerBase):
    created_by_user_id: int


class PlayerCreateOwn(PlayerBase):
    pass


class PlayerUpdate(PlayerBase):
    pass


class Player(PlayerBase):
    id: int
    user_id: Optional[int]
    created_by_user_id: int

    class Config:
        orm_mode = True
