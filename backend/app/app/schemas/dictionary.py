from app.schemas.base import BaseSchema


class DictionaryCategoryBase(BaseSchema):
    name: str


class DictionaryCategory(DictionaryCategoryBase):
    id: int

    class Config:
        orm_mode = True


class DictionaryCategoryCreate(BaseSchema):
    pass


class DictionaryCategoryUpdate(BaseSchema):
    id: int
