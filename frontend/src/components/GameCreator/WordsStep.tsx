import React, {useEffect, useState} from 'react'
import {StepComponentProps} from "./types";
import {StepHeader} from "./StepHeader";
import {Box, FormControl, InputLabel, MenuItem, Select, TextField} from "@mui/material";
import {DictionaryCategory, DictionaryService} from "../../api";
import {NumericInput} from "../../components-ui/NumericInput";

interface Props extends StepComponentProps {
    gameCategories: number[]
    setGameCategories: React.Dispatch<React.SetStateAction<number[]>>
    wordsCount: number
    setWordsCount: React.Dispatch<React.SetStateAction<number>>
}

export function WordsStep({gameCategories, setGameCategories, wordsCount, setWordsCount, renderFooter}: Props) {
    const [categories, setCategories] = useState<DictionaryCategory[]|null>(null)

    useEffect(() => {
        DictionaryService.readCategories().then(setCategories)
    }, [])

    if (categories === null) {
        return null
    }

    function validate() {
        if (gameCategories.length === 0) {
            return 'Выберите категории слов '
        }
    }

    return (
        <>
            <StepHeader>Выбор слов</StepHeader>

            <Box sx={{textAlign: 'left'}}>
                <FormControl fullWidth sx={{textAlign: 'left', mb: '20px'}}>
                    <InputLabel id="select-categories">Категории слов</InputLabel>
                    <Select
                        labelId="select-categories"
                        value={gameCategories}
                        label="Категории слов"
                        onChange={(e) => setGameCategories(e.target.value as number[])}
                        multiple
                    >
                        {categories.map(category => (
                            <MenuItem value={category.id} key={category.id}>
                                {category.name}
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>

                <NumericInput
                    label="Количество слов"
                    min={1}
                    max={100}
                    value={wordsCount}
                    onChange={(e, wordsCount) => setWordsCount(wordsCount)}
                />
            </Box>

            {renderFooter(validate)}
        </>
    )
}