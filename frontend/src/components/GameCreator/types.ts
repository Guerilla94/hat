import {Player} from "../../api";
import {ReactNode} from "react";

export type StepFinishingHandler = () => Promise<void>

export type ValidateHandler = () => string|void

export interface GamePlayer extends Player {
    team: string
}

export interface SelectedPlayer {
    alias: string
    team: string
}

export type ChangeSelectedPlayer = (playerNumber: number, playerFields: Partial<SelectedPlayer>) => void

export interface StepComponentProps {
    renderFooter: (validator?: ValidateHandler, handleStepFinishing?: StepFinishingHandler) => ReactNode
}