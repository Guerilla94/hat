import React from "react";
import {StepComponentProps} from "./types";
import {StepHeader} from "./StepHeader";
import {Stage} from "../../api";
import _ from "lodash"
import {Box, Button, FormControl, FormHelperText, IconButton, InputLabel, MenuItem, Select} from "@mui/material";
import {Delete} from "@mui/icons-material";

interface Props extends StepComponentProps {
    stages: Stage[]
    gameStages: number[]
    setGameStages:  React.Dispatch<React.SetStateAction<number[]>>
}

export function StagesStep({stages, gameStages, setGameStages, renderFooter}: Props) {

    function setSelectedStage(stageIndex: number, stageId: number) {
        setGameStages(selectedStages => {
            selectedStages = selectedStages.slice()

            // Если новый этап уже выбран, то указываем вместо него прежний этап. Таким образом, этапы будут заменены
            // местами, чтобы сохранить уникальность выбранных этапов
            const newStageIndex = selectedStages.indexOf(stageId)
            if (newStageIndex > -1) {
                selectedStages[newStageIndex] = selectedStages[stageIndex]
            }

            selectedStages[stageIndex] = stageId

            return selectedStages
        })
    }

    function deleteSelectedStage(stageIndex: number) {
        setGameStages(selectedStages => {
            selectedStages = selectedStages.slice()
            selectedStages.splice(stageIndex, 1)
            return selectedStages
        })
    }

    function addSelectedStage() {
        setGameStages(selectedStages => {
            const notSelectedStages = _.chain(stages)
                .map(stage => stage.id)
                .difference(selectedStages)
                .value()

            return [...selectedStages, notSelectedStages[0]]
        })
    }

    return (
        <>
            <StepHeader>Выбор этапов</StepHeader>

            {gameStages.map((selectedStageId, stageIndex) =>  {
                return (
                    <Box sx={{display: 'flex', alignItems: 'center', mb: '20px'}}>
                        <FormControl fullWidth key={selectedStageId} sx={{textAlign: "left"}}>
                            <InputLabel id={'select-stage-label-' + stageIndex}>{'Этап ' + (stageIndex + 1)}</InputLabel>
                            <Select
                                labelId={'select-stage-label-' + stageIndex}
                                value={selectedStageId}
                                label={'Этап ' + (stageIndex + 1)}
                                onChange={(e) => setSelectedStage(stageIndex, e.target.value as number)}
                            >
                                {stages.map(stage => (
                                    <MenuItem value={stage.id} key={stage.id}>
                                        {stage.name}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>

                        <IconButton
                            disabled={gameStages.length === 1}
                            sx={{ml: '5px'}}
                            onClick={() => deleteSelectedStage(stageIndex)}
                        >
                            <Delete />
                        </IconButton>
                    </Box>
                )
            })}

            {gameStages.length < stages.length && (
                <Button
                    variant="contained"
                    sx={{
                        mt: '25px',
                        width: '100%'
                    }}
                    onClick={addSelectedStage}
                >
                    Добавить этап
                </Button>
            )}

            {renderFooter()}
        </>
    )
}