import {ValidateHandler} from "./types";
import {Box, Button, Dialog, DialogActions, DialogContent, DialogContentText} from "@mui/material";
import LoadingButton from "@mui/lab/LoadingButton";
import React, {useState} from "react";

interface Props {
    isFirstStep: boolean
    isFinalStep: boolean

    validate?: ValidateHandler
    onSwitchPrevStep: () => void
    onFinishStep: () => Promise<void>|void
}

export function StepFooter({isFirstStep, isFinalStep, validate, onSwitchPrevStep, onFinishStep}: Props) {
    const [finishingLoading, setFinishingLoading] = useState(false)

    const [openValidationDialog, setOpenValidationDialog] = useState(false)
    const [validationMessage, setValidationMessage] = useState('')

    function handleClickFinishStep() {
        if (validate) {
            const message = validate()
            if (message) {
                setValidationMessage(message)
                setOpenValidationDialog(true)
                return
            }
        }

        const promise = onFinishStep()
        if (promise) {
            setFinishingLoading(true)
            promise.finally(() => setFinishingLoading(false))
        }
    }

    const handleValidationDialogClose = () => setOpenValidationDialog(false)

    return (
        <Box sx={{display: 'flex', mt: '20px'}}>
            {!isFirstStep && (
                <Button variant="outlined" onClick={onSwitchPrevStep}>
                Назад
                </Button>
            )}

            <LoadingButton variant="outlined" loading={finishingLoading} onClick={handleClickFinishStep} sx={{ml: 'auto'}}>
                {isFinalStep ? 'Начать игру' : 'Далее'}
            </LoadingButton>

            <Dialog
                open={openValidationDialog}
                onClose={handleValidationDialogClose}
                aria-describedby="validation-dialog-description"
            >
                <DialogContent>
                    <DialogContentText id="validation-dialog-description">
                        {validationMessage}
                    </DialogContentText>
                </DialogContent>

                <DialogActions>
                    <Button onClick={handleValidationDialogClose}>Ок</Button>
                </DialogActions>
            </Dialog>
        </Box>
    )
}