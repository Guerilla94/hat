import React, {ReactNode, useState} from "react";
import Container from "@mui/material/Container";
import {Paper} from "@mui/material";
import {PlayersStep} from "./PlayersStep";
import {GamePlayer, StepFinishingHandler, ValidateHandler} from "./types";
import {StagesStep} from "./StagesStep";
import {StepFooter} from "./StepFooter";
import {useGameStages} from "./hooks/useGameStages";
import {useSteps} from "./hooks/useSteps";
import {WordsStep} from "./WordsStep";
import {GamesService} from "../../api";

export default function GameCreator() {
    const {currentStepCode, isFirstStep, isFinalStep, switchToPrevStep, switchToNextStep} = useSteps(steps)

    const [gamePlayers, setGamePlayers] = useState<GamePlayer[]>([])
    const {stages, gameStages, setGameStages} = useGameStages()
    const [gameCategories, setGameCategories] = useState<number[]>([])
    const [wordsCount, setWordsCount] = useState<number>(30)

    if (!stages) {
        return null;
    }

    const createGame = () => {
        GamesService.createGame({
            players: gamePlayers.map(gamePlayer => ({
                player_id: gamePlayer.id,
                team_name: gamePlayer.team
            })),
            stages_ids: gameStages,
            dictionary_categories_ids: gameCategories,
            words_count: wordsCount
        })
    }

    const renderFooter = (validator?: ValidateHandler, handleStepFinishing?: StepFinishingHandler) => {
        async function handleFinishStep() {
            if (handleStepFinishing) {
                await handleStepFinishing()
            }

            if (isFinalStep) {
                createGame()
            } else {
                switchToNextStep()
            }
        }

        return (
            <StepFooter
                isFirstStep={isFirstStep}
                isFinalStep={isFinalStep}
                validate={validator}
                onSwitchPrevStep={switchToPrevStep}
                onFinishStep={handleFinishStep}
            />
        )
    }

    let stepComponent: ReactNode|null = null;
    switch (currentStepCode) {
        case 'players':
            stepComponent = <PlayersStep renderFooter={renderFooter} initialGamePlayers={gamePlayers} setGamePlayers={setGamePlayers}/>
            break;

        case 'stages':
            stepComponent = <StagesStep renderFooter={renderFooter} stages={stages} gameStages={gameStages} setGameStages={setGameStages}/>
            break;

        case 'words':
            stepComponent = <WordsStep
                renderFooter={renderFooter}
                gameCategories={gameCategories}
                setGameCategories={setGameCategories}
                wordsCount={wordsCount}
                setWordsCount={setWordsCount}
            />
    }

    return (
        <Container maxWidth="xs">
            <Paper
                variant="outlined"
                sx={{
                    padding: '20px'
                }}
            >
                {stepComponent}
            </Paper>
        </Container>
    )
}

const steps = ['players', 'stages', 'words']