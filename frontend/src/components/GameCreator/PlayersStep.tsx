import React, {useEffect, useState} from "react";
import {Player, UsersService} from "../../api";
import {Button, Checkbox, FormControlLabel} from "@mui/material";
import {ChangeSelectedPlayer, GamePlayer, SelectedPlayer, StepComponentProps} from "./types";
import _ from 'lodash'
import {PlayerRow} from "./PlayerRow";
import {StepHeader} from "./StepHeader";

const MIN_GAME_PLAYERS_COUNT = 2

interface Props extends StepComponentProps {
    initialGamePlayers: GamePlayer[]
    setGamePlayers: (players: GamePlayer[]) => void
}

export function PlayersStep({renderFooter, initialGamePlayers, setGamePlayers}: Props) {
    function getInitialSelectedPlayers(): SelectedPlayer[] {
        let initialPlayers

        if (initialGamePlayers.length > 0) {
            initialPlayers = initialGamePlayers.map(gamePlayer => _.pick(gamePlayer, ['alias', 'team']))
        } else {
            initialPlayers = []

            for (let i = 0; i < MIN_GAME_PLAYERS_COUNT; i++) {
                initialPlayers.push({alias: '', team: ''} as SelectedPlayer)
            }
        }

        return initialPlayers;
    }

    const [selectedPlayers, setSelectedPlayers] = useState<SelectedPlayer[]>(getInitialSelectedPlayers)
    const selectedPlayersAliases = _.map(selectedPlayers, 'alias')

    const teamsOptions = _
        .chain(selectedPlayers)
        .map('team')
        .filter()
        .uniq()
        .sort()
        .value()

    const [useTeams, setUseTeams] = useState(teamsOptions.length > 0)

    // список существующих игроков

    const [existingPlayers, setExistingPlayers] = useState<Player[]|null>(null)

    useEffect(() => {
        UsersService.getPlayers().then(players => setExistingPlayers(players))
    }, [])

    if (!existingPlayers) {
        return null;
    }

    const existingPlayersAliases = existingPlayers.map(player => player.alias)
    const playersOptions = existingPlayersAliases.filter(alias => !selectedPlayersAliases.includes(alias))

    //

    function handleClickAddPlayer() {
        setSelectedPlayers(
            players => [...players, {alias: '', team: ''}]
        )
    }

    const changeSelectedPlayer: ChangeSelectedPlayer = (playerNumber, playerFields) =>  {
        setSelectedPlayers(players => {
            players = players.slice();
            players[playerNumber] = {...players[playerNumber], ...playerFields}
            return players
        })
    }

    function deleteSelectedPlayer(playerNumber: number): void {
        setSelectedPlayers(players => {
            players = players.slice()
            players.splice(playerNumber, 1)
            return players
        })
    }

    const isSelectedMinPlayersCount = selectedPlayersAliases.length === MIN_GAME_PLAYERS_COUNT

    function validate() {
        if (selectedPlayersAliases.some(alias => alias.length === 0)) {
            return 'Не для всех игроков указаны имена. Укажите имена, либо удалите игроков с незаполненными именами'
        }

        if (_.uniq(selectedPlayersAliases).length !== selectedPlayersAliases.length) {
            return 'Для некоторых игроков указаны повторяющиеся имена. Для каждого игрока нужно указать уникальное имя'
        }

        const teams = _.map(selectedPlayers, 'team')
        const filledTeams = teams.filter(team => team.length > 0)

        if (useTeams) {
            if (filledTeams.length < selectedPlayers.length) {
                return 'Не для всех игроков указаны команды. Укажите команды для всех игроков'
            }

            const hasSinglePlayerCommands = _
                .chain(filledTeams)
                .countBy()
                .pickBy(teamMembersCount => teamMembersCount === 1)
                .keys()
                .value()
                .length > 0

            if (hasSinglePlayerCommands) {
                return 'Не во всех командах достаточное количество игроков. В каждую команду должны входить минимум два игрока'
            }
        }
    }

    async function handleStepFinishing() {
        const newPlayers = _
            .difference(selectedPlayersAliases, existingPlayersAliases)
            .map(alias => ({alias}))

        let createdPlayers: Player[] = [];
        if (newPlayers.length > 0) {
            createdPlayers = await UsersService.createPlayers(newPlayers)
        }

        const allExistingPlayers = (existingPlayers as Player[]).concat(createdPlayers)
        const allExistingPlayersMap = _.keyBy(allExistingPlayers, 'alias')

        const gamePlayers = selectedPlayers.map(selectedPlayer => {
            const player = allExistingPlayersMap[selectedPlayer.alias]
            return {
                ...player,
                team: useTeams ? selectedPlayer.team : ''
            }
        })

        setGamePlayers(gamePlayers)
    }

    return (
        <>
            <StepHeader>Выбор игроков</StepHeader>

            <FormControlLabel
                control={
                    <Checkbox checked={useTeams} onChange={(e, checked) => setUseTeams(checked)}/>
                }
                label="Разделить игроков по командам"
            />

            {selectedPlayers.map((player, playerNumber) => (
                <PlayerRow
                    key={playerNumber}
                    player={player}
                    playerNumber={playerNumber}
                    playersOptions={playersOptions}
                    showTeam={useTeams}
                    teamsOptions={teamsOptions}
                    disabledDeletion={isSelectedMinPlayersCount}

                    changeSelectedPlayer={changeSelectedPlayer}
                    deleteSelectedPlayer={deleteSelectedPlayer}
                />
            ))}

            <Button
                variant="contained"
                sx={{
                    mt: '25px',
                    width: '100%'
                }}
                onClick={handleClickAddPlayer}
            >
                Добавить игрока
            </Button>

            {renderFooter(validate, handleStepFinishing)}
        </>
    )
}