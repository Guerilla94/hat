import React, {ReactNode} from "react";
import Typography from "@mui/material/Typography";

interface Props {
    children: ReactNode
}

export function StepHeader({children}: Props) {
    return (
        <Typography component="h1" variant="h5" sx={{mb: '20px'}}>{children}</Typography>
    )
}