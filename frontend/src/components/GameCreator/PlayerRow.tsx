import React from "react";
import {Autocomplete, Box, IconButton} from "@mui/material";
import TextField from "@mui/material/TextField";
import _ from "lodash";
import {Delete} from "@mui/icons-material";
import {ChangeSelectedPlayer, SelectedPlayer} from "./types";

interface Props {
    player: SelectedPlayer
    playerNumber: number
    playersOptions: string[]
    showTeam: boolean
    teamsOptions: string[]
    disabledDeletion: boolean

    changeSelectedPlayer: ChangeSelectedPlayer
    deleteSelectedPlayer: (playerNumber: number) => void
}

export function PlayerRow(props: Props) {
    const {
        player, playerNumber, playersOptions, showTeam, teamsOptions, disabledDeletion, changeSelectedPlayer,
        deleteSelectedPlayer
    } = props

    return (
        <Box sx={{display: 'flex', alignItems: 'center', mb: '5px'}}>
            <Autocomplete
                options={playersOptions}
                renderInput={params => (
                    <TextField
                        {...params}
                        margin="normal"
                        required
                        fullWidth
                        label={`Игрок ${playerNumber + 1}`}
                    />
                )}
                freeSolo
                sx={{width: '100%', mr: '10px'}}

                inputValue={player.alias}
                onInputChange={(e, alias) => changeSelectedPlayer(playerNumber, {alias})}
            />

            {showTeam && (
                <Autocomplete
                    options={_.without(teamsOptions, player.team)}
                    renderInput={params => (
                        <TextField
                            {...params}
                            margin="normal"
                            required
                            fullWidth
                            label="Команда"
                            inputProps={{...params.inputProps, maxLength: 100}}
                        />
                    )}
                    freeSolo
                    sx={{width: '100%'}}

                    inputValue={player.team}
                    onInputChange={(e, team) => changeSelectedPlayer(playerNumber, {team})}
                />
            )}

            <IconButton
                disabled={disabledDeletion}
                sx={{ml: '5px'}}
                onClick={() => deleteSelectedPlayer(playerNumber)}
            >
                <Delete />
            </IconButton>
        </Box>
    )
}