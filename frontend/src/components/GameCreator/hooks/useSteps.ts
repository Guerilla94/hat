import {useState} from "react";

export function useSteps(steps: string[]) {
    const [currentStepIndex, setCurrentStepIndex] = useState(0)
    const currentStepCode = steps[currentStepIndex]
    const isFirstStep = currentStepIndex === 0
    const isFinalStep = currentStepIndex === steps.length - 1

    const switchToPrevStep = () => {
        setCurrentStepIndex(currentStepIndex => currentStepIndex - 1)
    }

    const switchToNextStep = () => {
        setCurrentStepIndex(currentStepIndex => currentStepIndex + 1)
    }

    return {currentStepCode, isFirstStep, isFinalStep, switchToPrevStep, switchToNextStep}
}