import {useEffect, useState} from "react";
import {Stage, StagesService} from "../../../api";
import _ from "lodash";

export function useGameStages() {
    const [gameStages, setGameStages] = useState<number[]>([])
    const [stages, setStages] = useState<Stage[]|null>(null)

    useEffect(() => {
        StagesService.readStages().then(function (stages) {
            setStages(stages)

            const initialGameStages = _.chain(stages)
                .filter(stage => stage.default_order !== undefined)
                .orderBy(stage => stage.default_order)
                .map(stage => stage.id)
                .value()

            setGameStages(initialGameStages)
        })
    }, [])

    return {stages, gameStages, setGameStages}
}