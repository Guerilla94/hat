import React from 'react'
import {Avatar as AvatarMaterial} from "@mui/material";
import {User} from "../../api";

interface Props {
    user: User
}

export default function Avatar({user}: Props) {
    return (
        <AvatarMaterial sx={{ width: 32, height: 32 }}>
            {user.first_name?.[0]}
        </AvatarMaterial>
    );
}