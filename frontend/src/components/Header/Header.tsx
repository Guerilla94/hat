import React from "react";
import Box from "@mui/material/Box";
import Logo from "./Logo";
import MainMenu from "./MainMenu";
import UserPanel from "./UserPanel";

export default function Header() {
    return (
        <React.Fragment>
            <Box sx={{
                display: 'flex',
                alignItems: 'center',
                textAlign: 'center',
                padding: '10px 20px',
                justifyContent: 'space-between'
            }}>
                <Box sx={{display: 'flex', alignItems: 'center'}}>
                    <Logo />
                    <MainMenu />
                </Box>

                <UserPanel />
            </Box>
        </React.Fragment>
    );
}