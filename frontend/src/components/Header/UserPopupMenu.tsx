import React from "react";
import {Menu, MenuItem, ListItemIcon} from "@mui/material";
import Logout from '@mui/icons-material/Logout';
import {logout} from "../../auth/auth";

interface Props {
    anchor: HTMLElement | null
    isOpened: boolean
    close: () => void
}

export default function ({anchor, isOpened, close}: Props) {
    return (
        <Menu
            anchorEl={anchor}
            id="account-menu"
            open={isOpened}
            onClick={close}
            onClose={close}
            PaperProps={{
                elevation: 0,
                sx: {
                    overflow: 'visible',
                    filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
                    mt: 1.5,
                    '& .MuiAvatar-root': {
                        width: 32,
                        height: 32,
                        ml: -0.5,
                        mr: 1,
                    },
                    '&:before': {
                        content: '""',
                        display: 'block',
                        position: 'absolute',
                        top: 0,
                        right: 14,
                        width: 10,
                        height: 10,
                        bgcolor: 'background.paper',
                        transform: 'translateY(-50%) rotate(45deg)',
                        zIndex: 0,
                    },
                },
            }}
            transformOrigin={{ horizontal: 'right', vertical: 'top' }}
            anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
        >
            <MenuItem onClick={logout}>
                <ListItemIcon>
                    <Logout fontSize="small"/>
                </ListItemIcon>
                Выйти
            </MenuItem>
        </Menu>
    )
}