import {Box, IconButton, Tooltip, Typography} from "@mui/material";
import Avatar from "./Avatar";
import React, {useContext} from "react";
import UserContext from "../../common/UserContext";
import UserPopupMenu from "./UserPopupMenu";

export default function UserPanel() {

    const user = useContext(UserContext);

    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const isOpened = Boolean(anchorEl);

    const openMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const closeMenu = () => {
        setAnchorEl(null);
    };

    return (
        <Box sx={{display: 'flex', alignItems: 'center'}}>
            <Typography>{user.first_name} {user.last_name}</Typography>

            <Tooltip title="Account settings">
                <IconButton
                    onClick={openMenu}
                    size="small"
                    sx={{ ml: 2 }}
                    aria-controls={isOpened ? 'account-menu' : undefined}
                    aria-haspopup="true"
                    aria-expanded={isOpened ? 'true' : undefined}
                >
                    <Avatar user={user}/>
                </IconButton>
            </Tooltip>

            <UserPopupMenu
                anchor={anchorEl}
                isOpened={isOpened}
                close={closeMenu}
            />
        </Box>
    )
}