import {Typography} from "@mui/material";
import React from "react";

export default function Logo() {
    return (
        <Typography sx={{fontSize: '2rem'}}>Полная Шляпа</Typography>
    );
}