import React from "react";
import {Box, Typography} from '@mui/material'

export default function MainMenu() {
    return (
        <Box sx={{display: 'flex'}}>
            <Typography sx={{ minWidth: 200}}>Начать игру!</Typography>
            <Typography sx={{ minWidth: 200}}>Прошедшие игры</Typography>
        </Box>
    );
}