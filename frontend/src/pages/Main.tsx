import React from "react";
import Header from "../components/Header/Header";
import GameCreator from "../components/GameCreator";

export default function Main() {
    return (
        <React.Fragment>
            <Header />
            <GameCreator/>
        </React.Fragment>
    )
}