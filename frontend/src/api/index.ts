/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export { ApiError } from './core/ApiError';
export { CancelablePromise, CancelError } from './core/CancelablePromise';
export { OpenAPI } from './core/OpenAPI';
export type { OpenAPIConfig } from './core/OpenAPI';

export type { Body_login_login_access_token } from './models/Body_login_login_access_token';
export type { Body_login_reset_password } from './models/Body_login_reset_password';
export type { Body_users_create_user_open } from './models/Body_users_create_user_open';
export type { Body_users_update_user_me } from './models/Body_users_update_user_me';
export type { DictionaryCategory } from './models/DictionaryCategory';
export type { GameCreate } from './models/GameCreate';
export type { GamePlayerCreate } from './models/GamePlayerCreate';
export type { HTTPValidationError } from './models/HTTPValidationError';
export type { Msg } from './models/Msg';
export type { Player } from './models/Player';
export type { PlayerCreateOwn } from './models/PlayerCreateOwn';
export type { Stage } from './models/Stage';
export type { Token } from './models/Token';
export type { User } from './models/User';
export type { UserCreate } from './models/UserCreate';
export type { UserUpdate } from './models/UserUpdate';
export type { ValidationError } from './models/ValidationError';

export { DictionaryService } from './services/DictionaryService';
export { GamesService } from './services/GamesService';
export { LoginService } from './services/LoginService';
export { StagesService } from './services/StagesService';
export { UsersService } from './services/UsersService';
export { UtilsService } from './services/UtilsService';
