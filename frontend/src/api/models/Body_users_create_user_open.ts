/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Body_users_create_user_open = {
    password: string;
    email: string;
    first_name?: string;
    last_name?: string;
};
