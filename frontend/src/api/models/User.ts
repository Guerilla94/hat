/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type User = {
    email?: string;
    is_active?: boolean;
    is_superuser?: boolean;
    first_name?: string;
    last_name?: string;
    id?: number;
};
