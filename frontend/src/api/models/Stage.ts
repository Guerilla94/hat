/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Stage = {
    name: string;
    description: string;
    default_order?: number;
    id: number;
};
