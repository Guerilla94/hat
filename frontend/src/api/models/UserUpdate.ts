/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type UserUpdate = {
    email?: string;
    is_active?: boolean;
    is_superuser?: boolean;
    first_name?: string;
    last_name?: string;
    password?: string;
};
