/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Player = {
    alias: string;
    id: number;
    user_id?: number;
    created_by_user_id: number;
};
