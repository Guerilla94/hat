/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Body_users_update_user_me = {
    password?: string;
    first_name?: string;
    last_name?: string;
    email?: string;
};
