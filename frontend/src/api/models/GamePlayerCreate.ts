/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type GamePlayerCreate = {
    player_id: number;
    team_name: string;
};
