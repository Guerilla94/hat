/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { GamePlayerCreate } from './GamePlayerCreate';

export type GameCreate = {
    players: Array<GamePlayerCreate>;
    stages_ids: Array<number>;
    dictionary_categories_ids: Array<number>;
    words_count: number;
};
