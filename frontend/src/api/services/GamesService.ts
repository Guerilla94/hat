/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { GameCreate } from '../models/GameCreate';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class GamesService {

    /**
     * Create Game
     * @param requestBody
     * @returns any Successful Response
     * @throws ApiError
     */
    public static createGame(
        requestBody: GameCreate,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/api/v1/games/',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                422: `Validation Error`,
            },
        });
    }

}