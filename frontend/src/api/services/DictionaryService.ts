/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { DictionaryCategory } from '../models/DictionaryCategory';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class DictionaryService {

    /**
     * Read Categories
     * @returns DictionaryCategory Successful Response
     * @throws ApiError
     */
    public static readCategories(): CancelablePromise<Array<DictionaryCategory>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/api/v1/dictionary/',
        });
    }

}