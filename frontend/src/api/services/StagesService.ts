/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Stage } from '../models/Stage';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class StagesService {

    /**
     * Read Stages
     * @returns Stage Successful Response
     * @throws ApiError
     */
    public static readStages(): CancelablePromise<Array<Stage>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/api/v1/stages/',
        });
    }

}