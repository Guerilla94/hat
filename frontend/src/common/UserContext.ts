import React from "react";
import {User} from "../api";

const UserContext = React.createContext<User>({});
export default UserContext;