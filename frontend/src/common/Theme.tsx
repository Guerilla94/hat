import React from "react";
import {createTheme, ThemeProvider} from "@mui/material/styles";
import {LinkProps, Link} from 'react-router-dom'

// use Link from react router as MUI link
const LinkBehavior = React.forwardRef<
        any,
        Omit<LinkProps, 'to'> & { href: LinkProps['to'] }
    >((props, ref) => {
        const { href, ...other } = props;
        // Map href (MUI) -> to (react-router)
        return <Link ref={ref} to={href} {...other} />;
    });

const theme = createTheme({
    components: {
        MuiLink: {
            defaultProps: {
                // @ts-ignore
                component: LinkBehavior,
            },
        },
        MuiButtonBase: {
            defaultProps: {
                LinkComponent: LinkBehavior,
            },
        },
    },
});

export default function Theme({children}: any) {
    return <ThemeProvider theme={theme} children={children}/>
}