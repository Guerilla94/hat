import React, {useContext, useEffect, useState} from 'react';
import './App.css';
import {User, UsersService} from "./api";
import AppRoutes from "./routes/AppRoutes";
import Theme from "./common/Theme";

interface AuthData {
    user?: User | null
    loadUser: () => Promise<null>
}

const AuthContext = React.createContext<AuthData>({
    loadUser: () => new Promise<null>(() => null)
});

export const useAuthData = () => useContext(AuthContext);

function App() {

    const [user, setUser] = useState<User|null>();

    const loadUser = () => {
        return new Promise<null>(function (resolve) {
            UsersService.readUserMe().then(function (user) {
                setUser(user)
                resolve(null)
            })
        })
    }

    useEffect(() => {
        loadUser()
    }, [])

    useEffect(() => {
        function handleRejection(event: PromiseRejectionEvent) {
            if (event.reason.status === 401) {
                setUser(null)
            }
        }

        window.addEventListener('unhandledrejection', handleRejection);

        return () => window.removeEventListener('unhandledrejection', handleRejection)
    }, [])

    if (user === undefined) {
        return null;
    }

    return (
        <Theme>
            <AuthContext.Provider value={{user, loadUser}}>
                <div className="App">
                    <AppRoutes/>
                </div>
            </AuthContext.Provider>
        </Theme>
    );
}

export default App;
