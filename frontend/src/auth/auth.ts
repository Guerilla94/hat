import {OpenAPI} from "../api";

export function setToken(token: string) {
    window.localStorage.setItem('accessToken', token);
    OpenAPI.TOKEN = token;
}

export function getToken(): string | null {
    return window.localStorage.getItem('accessToken');
}

export function logout(): void {
    window.localStorage.removeItem('accessToken')
    window.location.href = '/sign-in'
}