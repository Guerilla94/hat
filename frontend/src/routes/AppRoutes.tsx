import React from "react";
import {Route, Routes} from "react-router-dom";
import Protected from "./Protected";
import Main from "../pages/Main";
import SignIn from "../pages/SignIn";
import SignUp from "../pages/SignUp";

export default function AppRoutes() {
    return (
        <Routes>
            <Route path="/" element={<Protected><Main/></Protected>}/>
            <Route path="/sign-in" element={<SignIn/>}/>
            <Route path="/sign-up" element={<SignUp/>}/>
        </Routes>
    )
}