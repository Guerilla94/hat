import React, {ReactElement} from "react";
import {useAuthData} from "../App";
import {Navigate} from "react-router-dom";
import UserContext from "../common/UserContext";

type Props = {
    children: ReactElement,
}

export default function Protected({children}: Props) {
    const {user} = useAuthData()

    if (!user) {
        return <Navigate to="/sign-in" />
    }

    return (
        <UserContext.Provider value={user}>
            {children}
        </UserContext.Provider>
    );
}