import React, {ChangeEvent} from "react";
import {TextField} from "@mui/material";

interface Props {
    label: string | number
    min?: number
    max?: number
    value: number
    onChange: (e: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>, value: number) => void
}

export function NumericInput(props: Props) {
    const {label, min, max, value, onChange} = props

    function handleChange(e: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) {
        const clearedValue = e.target.value.replaceAll(/[^0-9]/g, '')
        let numberValue = Number(clearedValue)

        if (min !== undefined && numberValue < min) {
            numberValue = min
        } else if (max !== undefined && numberValue > max) {
            numberValue = max
        }

        onChange(e, numberValue)
    }

    return (
        <TextField label={label} value={value} onChange={handleChange}/>
    )
}